#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
Created on 4 Aug 2017

@author: henrikahl
'''
import os
import numpy as np
import vtk
import Meristem_Phenotyper_3D as ap
import pandas as pd
import copy
import handy_functions as hf
import attractor_processing as boaa
import networkx as nx
from skimage import measure
from vtk.util import numpy_support as nps
import tifffile as tiff
from scipy.ndimage.morphology import binary_opening, binary_closing
from scipy.ndimage.filters import gaussian_filter
from skimage.segmentation import morphological_chan_vese, morphological_geodesic_active_contour, inverse_gaussian_gradient, checkerboard_level_set
from scipy.ndimage.morphology import binary_fill_holes
home = os.path.expanduser('~')

''' FILE INPUT '''
#dir_ = os.path.abspath(os.path.join(home, 'projects', 'Meristem_Phenotyper_3D'))
#file_ = os.path.abspath(os.path.join(dir_, 'test_images', 'meristem_test.tif'))
#file_ = os.path.abspath(os.path.join(home, 'data', 'plant2', "00hrs_plant2_trim-acylYFP_hmin_2_asf_1_s_1.50_clean_3.tif")) # NPA meristem
#file_ = os.path.abspath(os.path.join(home, 'data', "20171102-FM4-64-cmu1cmu2-2-15_sam.tif")) # CMU1CMU2 mutant
#file_ = os.path.abspath(os.path.join(home, 'data', "20171103-FM4-64-Col0-2-15_sam.tif")) # WT mutant
#file_ = os.path.abspath(os.path.join(home, 'data', "C2-WUS-GFP-24h-light-1-1-Soil-1.tif")) # One of Benoit's
#file_ = os.path.abspath(os.path.join(home, 'data', "C2-WUS-GFP-24h-light-2-3-Soil-1-3-Sand-9.tif")) # Another of Benoits. Worse quality.

file_= '/home/henrik/projects/stackAlign/data/pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-2-0h-mCherry-0.7-Gain-800_topcrop.tif'
file_ = '/home/henrik/pWUS-3XVENUS-pCLV3-mCherry-off_NPA-5-18h-2.0-750-0.5-770.tif'
data = tiff.imread(file_)
wus = data[:, 0]
clv3 = data[:, 1]
fluo = data[:, 2] # Take out autofluorescence


''' Create AutoPhenotype object to store the data in '''
A = ap.AutoPhenotype()
A.data = fluo

''' Smooth the data (to fill holes) and create a contour. lambda2 > lambda1:
  more variable on inside. Smoothing might have to be corrected for different
  xy-z dimensionality. Iterations should ideally be at least 10, smoothing
  around 4. '''
A.data = gaussian_filter(A.data, sigma=[3, 3, 3])
A.data = gaussian_filter(A.data, sigma=[3, 3, 3])
A.data = gaussian_filter(A.data, sigma=[3, 3, 3])
contour = morphological_chan_vese(A.data.astype(np.uint8), iterations=1,
                                  init_level_set=A.data > 1. * np.mean(A.data),
                                  smoothing=1, lambda1=1, lambda2=100)

''' Remove top slice. Normally we have a lot of noise here. Then fill holes
    inside contour. '''
A.contour = copy.deepcopy(contour)
A.contour[np.array([len(A.contour)-1])] = 0
A.contour = binary_fill_holes(A.contour)
A.contour = A.contour.astype(np.float)

''' Generate mesh '''
verts, faces, normals, values = measure.marching_cubes_lewiner(
    A.contour, 0, spacing=(0.22, 0.2516, 0.2516), step_size=4,
    allow_degenerate=True)

''' Port mesh to VTK '''
# Assign point data
points = vtk.vtkPoints()
points.SetData(nps.numpy_to_vtk(np.ascontiguousarray(verts),
                                array_type=vtk.VTK_FLOAT, deep=True))

# Create polygons
nFaces = len(faces)
faces = np.array([np.append(len(ii), ii) for ii in faces]).flatten()
polygons = vtk.vtkCellArray()
polygons.SetCells(nFaces, nps.numpy_to_vtk(faces, array_type=vtk.VTK_ID_TYPE))

# Create polydata from points and polygons
polygonPolyData = vtk.vtkPolyData()
polygonPolyData.SetPoints(points)
polygonPolyData.SetPolys(polygons)

''' Process mesh '''
A.mesh = polygonPolyData
A.clean_mesh()
A.show_mesh(opacity=5)
A.compute_normals()

A.clean_mesh()
A.fill_holes(50.0)
#A.show_normals(opacity=.5)
A.smooth_mesh(iterations=0, relaxation_factor=.1)
A.quadric_decimation(dec=2000.0, method="npoints")
A.clean_mesh()
A.smooth_mesh(iterations=300, relaxation_factor=.01, boundarySmoothing=False,
              featureEdgeSmoothing=False)

''' Get coordinates for points on mesh and create function for measuring the
    distance to them for other points. '''
#coords = nps.vtk_to_numpy(A.mesh.GetPoints().GetData())
coords = np.array([A.mesh.GetPoint(ii) for ii in xrange(A.mesh.GetNumberOfPoints())])
implicit_function = vtk.vtkImplicitPolyDataDistance()
poly = vtk.vtkPolyData()
poly.SetPoints(A.mesh.GetPoints())
implicit_function.SetInput(poly)

''' When looking at intensity data, only look inside the contour. Here we create
    a rectangle around this. We then input our intensity data to a VTK object.
    '''
# Find bounds
start = []
stop = []
for dd in xrange(contour.ndim):
  rolled = np.rollaxis(contour, dd)
  for ii in xrange(rolled.shape[0]):
    if np.any(rolled[ii] > 0):
      start.append(ii)
      break
  for ii in xrange(rolled.shape[0] - 1, -1, -1):
    if np.any(rolled[ii] > 0):
      stop.append(ii)
      break

# Convert the VTK array to vtkImageData
data = wus
data_cropped = data[start[0]:stop[0], start[1]:stop[1], start[2]:stop[2]].ravel()
vtk_data_array = nps.numpy_to_vtk(
  num_array=data_cropped,
  deep=True,
  array_type=vtk.VTK_FLOAT)

spacing = [0.22, 0.2516, 0.2516]
img_vtk = vtk.vtkImageData()
img_vtk.SetDimensions(np.subtract(stop, start))
img_vtk.SetOrigin(np.multiply(start, spacing))
img_vtk.SetSpacing([0.22, 0.2516, 0.2516])
img_vtk.GetPointData().SetScalars(vtk_data_array)
pts = np.array([img_vtk.GetPoint(ii) for ii in xrange(img_vtk.GetNumberOfPoints())])

data_line = data_cropped
pts = pts[data_line > 0]
data_line = data_line[data_line > 0]

#del data, spacing, vtk_data_array, verts, values, rolled, normals, faces
import gc; gc.collect()
d = np.zeros((len(pts), 3), dtype='float64')
dists = np.zeros(len(pts))
outpt = np.zeros((3,), dtype='float64')

for ii in xrange(len(pts)):
  dists[ii] = implicit_function.EvaluateFunctionAndGetClosestPoint(pts[ii], outpt)
  d[ii] = outpt

A.fill_holes(1000)
A.compute_normals()

pointsPolydata = vtk.vtkPolyData()
vpts = vtk.vtkPoints()
vpts.SetData(nps.numpy_to_vtk(pts, deep=1, array_type=vtk.VTK_FLOAT))
pointsPolydata.SetPoints(vpts)
extPts = vtk.vtkSelectEnclosedPoints()
extPts.SetInputData(pointsPolydata)

extPts.SetSurfaceData(A.mesh)
extPts.Update()
inside = [extPts.IsInside(ii) for ii in xrange(len(pts))]
inside = np.array(inside, dtype='bool')

data_line = data_line[inside]
pts = pts[inside]

from scipy.spatial import cKDTree

dists = np.zeros((len(pts), ))
idxs = np.zeros((len(pts), ))

tree = cKDTree(coords)
for ii in xrange(len(pts)):
  dists[ii], idxs[ii] = tree.query(pts[ii], k=1)

filter_ = dists < 6
dists = dists[filter_]
idxs = idxs[filter_]
data_line = data_line[filter_]

vals = np.zeros((A.mesh.GetNumberOfPoints(), ))
for ii in xrange(len(idxs)):
  vals[idxs[ii].astype(np.int)] += data_line[ii]

vtkVals = nps.numpy_to_vtk(vals, deep=True, array_type=vtk.VTK_FLOAT)

''' TODO: Send into plot point vals '''
neighs = np.array([ap.get_connected_vertices(A.mesh, ii) for ii in xrange(A.mesh.GetNumberOfPoints())]) # get linkage
vals   = np.array([np.mean(vals[np.append(ii, neighs[ii])]) for ii in xrange(A.mesh.GetNumberOfPoints())])
vals   = np.array([np.mean(vals[np.append(ii, neighs[ii])]) for ii in xrange(A.mesh.GetNumberOfPoints())])
vals   = np.array([np.mean(vals[np.append(ii, neighs[ii])]) for ii in xrange(A.mesh.GetNumberOfPoints())])

A.show_point_values(pd.DataFrame(vals), stdevs="all", logScale=True, ruler=True)


#########
#mapper = vtk.vtkDataSetMapper()
#mapper.SetInputData(img_vtk)
#actor = vtk.vtkActor()
#actor.SetMapper(mapper)
#
#renderWindow = vtk.vtkRenderWindow()
#
#renderer = vtk.vtkRenderer()
#renderWindow.AddRenderer(renderer)
#renderer.AddActor(actor)
#renderer.ResetCamera()
#renderWindowInteractor = vtk.vtkRenderWindowInteractor()
#renderWindowInteractor.SetRenderWindow(renderWindow)
#renderWindow.Render()
#renderWindowInteractor.Start()
#












### This is extremely slow v
#distances = np.zeros((len(pts), ))
#closestPts = np.zeros((len(pts),))
#import operator
#for ii in xrange(len(pts)):
#  distance = np.sqrt(np.sum((pts[ii] - coords)**2, axis=1))
#  min_index, min_value = min(enumerate(distance), key=operator.itemgetter(1))
#  distances[ii] = min_value
#  closestPts[ii] = min_index

#inside = [extPts.IsInsideSurface(ii) for ii in pts]


#d     = d[dists < 0]
#dists = dists[dists < 0]
#
#d     = d[dists > -6]
#dists = dists[dists > -6]

#assert(d.dtype == coords.dtype)
#
##matches = []
#count = 0
##for ii in d:
##  for jj in coords:
##    if np.all(ii == jj):
##      count += 1
##      break
#l = []
#for ii in d:
#  l.append(np.where(np.all(ii == coords, axis=1)))

#def check_inside(implicit_function, x,y,z):
#  return implicit_function.FunctionValue([x,y,z]) <= 0


#
##A.smooth_mesh(iterations=300, relaxation_factor=.01)
  #A.update_mesh()
#A.clean_mesh()
#
#A.show_mesh()
#dist_ = 1
#curvs = ap.get_curvatures(A, dist_=dist_, curv_types=['gauss', 'max'], operations=['-'], ignore_boundary=True)
#A.show_curvatures(stdevs="all", curvs=curvs)
#
#''' Create graphs '''
#nPoints = A.mesh.GetNumberOfPoints()
#coords = [A.mesh.GetPoint(ii) for ii in xrange(A.mesh.GetNumberOfPoints())]
#neighs = [ap.get_connected_vertices(A.mesh, ii) for ii in xrange(nPoints)]
#coords = pd.DataFrame(coords)
#neighs = pd.DataFrame(np.array(neighs))
#pointData = pd.concat([curvs, coords, neighs], axis=1)
#pointData.columns = ['curv', 'x', 'y', 'z', 'neighs']
#pointData['domain'] = np.NaN * nPoints
#
#''' Identify BoAs'''
#pointData      = boa.domains_from_curvature(pointData)
#boas, boasData = boa.get_boas(pointData)
##
##''' Process boas '''
#safeCopy = copy.deepcopy(pointData)
#pointData = copy.deepcopy(safeCopy)
#boas, boasData = boa.get_boas(pointData)

#pointData = boa.merge_boas_depth(pointData, threshold=0.008)
#pointData = boa.merge_boas_depth(pointData, threshold=0.010)
#boas, boasData = boa.get_boas(pointData)
#
#pointData = boa.merge_boas_engulfing(A, pointData, threshold=0.6)
#boas, boasData = boa.get_boas(pointData)
#
#pointData = boa.remove_boas_size(pointData, .10, method = "relative_meristem")
#boas, boasData = boa.get_boas(pointData)
##
##''' Visualise in VTK '''
##print boa.get_nBoas(pointData)
##print boa.get_boas_nPoints(pointData)
###A.show_curvatures(stdevs = "all", curvs = curvs)
#boaCoords = [tuple(x) for x in boasData[['x', 'y', 'z']].values]
#A.show_point_values(vals = pd.DataFrame(np.array(pointData['domain'])),
#                    discrete = True, boaCoords=boaCoords)