import pandas as pd
import numpy as np
import os
import matplotlib as plt
import fnmatch

home = os.path.expanduser('~')
folder = home + '/projects/yi_curvature'
resFolder = home + '/home3/yi_curvature_data/'
file_info = pd.read_csv(folder + "/file_info.dat",
                        sep='\s{2,}', header=None, comment="#", engine='python',
                        names=["file", "quality"])
file_info = pd.DataFrame(
    file_info[file_info.quality < 2])  # filter out bad ones

# TODO: We changed the location of the data files, so they're all located under
#       home3 now. This means that we must change the paths in file_info

''' List all the results files '''
resFiles = []
for root, dirnames, filenames in os.walk(resFolder):
    for filename in fnmatch.filter(filenames, '*results.csv'):
        resFiles.append(os.path.join(root, filename))

# TODO: Filter out low quality ones in resFiles


# TODO: Get mutant names
''' Get mutant names '''

''' Read results into a single data file '''
data = pd.DataFrame()
list_ = []
for file_ in resFiles:
    data = pd.read_csv(file_, header=0)
    list_.append(data)
data = pd.concat(list_)

# TODO: Extract the curvature values

# TODO: Make boxplot with Col0 in the beginning.
#       - high and low
#       - arithemtic mean
#       - geometric mean
