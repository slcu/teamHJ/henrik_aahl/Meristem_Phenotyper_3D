#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 14 14:38:43 2018

@author: henrik
"""
import numpy as np
import pandas as pd
import networkx as nx
import handy_functions as hf
import scipy
from vtk.util import numpy_support as nps
import vtk

# TODO: Documentation
def domains_from_curvature(pointData):
  # Connect the edges. In case of BoA, connect to one of the neighbours. Can
  # otherwise cause troubles if this point is a boa without any points pointing
  # to it, as it won't be included in the edge set, and hence not resulting
  # graphs, unless we do this
  G = nx.Graph()
  edges = []
  for key, value in enumerate(pointData['curv']):
      neighCurvs = pointData.loc[pointData['neighs'][key], 'curv']
      key_not_boa = len(neighCurvs.loc[neighCurvs >= value]) > 0
      if key_not_boa:
        connected = neighCurvs.loc[neighCurvs >= value].idxmax()
      else:
        connected = pointData.loc[pointData['neighs'][key], 'curv'].index[0]
      edges.append((key, connected))

  G.add_edges_from(edges)
  graphs = list(nx.connected_components(G))

  # Set domain values by subgraphs
  for key, graph in enumerate(graphs):
      gg = hf.set_to_numpy(graph)
      pointData.loc[gg, 'domain'] = key

  return pointData

# TODO: Documentation
def get_boas(pointData, disregardBoundary=False):
  # Take boas as the points withing domains with lowest curvature
  boas = []
  for ii in np.unique(pointData.domain.values):
    if not np.isnan(ii):
      index = pointData.loc[pointData.domain == ii, 'curv'].idxmax()
      boas.append(index)
  boas = np.array(boas)
  boasData = pointData.iloc[boas]
  return boas, boasData

# TODO: Documentation
def merge_boas_distance(pointData, boas, boasData, distance):
  # Find BoAs within certain distance of each other
  tree   = scipy.spatial.cKDTree(boasData[['x', 'y', 'z']])
  groups = tree.query_ball_point(boasData[['x', 'y', 'z']], distance)
  groups = hf.merge_lists_to_sets(groups)
  groups = np.array([hf.set_to_numpy(ii) for ii in groups])

  # Merge domains
  for key, value in enumerate(groups):
    if len(value) > 1:
      newDomain = pointData.loc[boas[value[0]], 'domain']
      targetDomains = pointData.loc[boas[value[1:]], 'domain']
      pointData.loc[pointData['domain'].isin(targetDomains), 'domain'] = newDomain
    # TODO: Update boas data
  return pointData

# TODO: Documentation
def merge_boas_valley(pointData, boas, threshold):
  # Create graph
  G = nx.Graph()
  edges = []

  # Make everything connect
  for key, value in enumerate(pointData['neighs']):
    for ii in value:
      edges.append(tuple([key, ii]))
  G.add_edges_from(edges)

  for ii in boas:
    for jj in boas:
      # Find shortest path between two seeds
      checkpoints = np.array(nx.shortest_path(G, ii, jj))
      # Find the minimum curvature value between them
      minCurv = pointData['curv'].iloc[checkpoints].min()

      # If this is higher than some threshold, merge them
      if minCurv > threshold:
        isInDomain = pointData.domain == pointData.loc[ii, 'domain']
        pointData.loc[isInDomain, 'domain'] = pointData.loc[jj, 'domain']
        # TODO: update boas / boasData
  return pointData

# TODO: THis piece of shit function
def merge_boas_engulfing(A, pointData, threshold=.9):
  changed = True
  while changed:
    changed = False # new round
    domains = pointData.domain.unique()

    ''' For every domain, find points facing other domains and domains facing NaN (the boundary) '''
    for ii in domains:
      inDomain = np.where(pointData.domain == ii)[0]
      inDomBoundary = np.intersect1d(inDomain, get_boundary_points(A))

      domainNeighs = np.unique([x for y in pointData.loc[inDomain, 'neighs'].values for x in y])
      neighDomains = pointData.loc[domainNeighs].loc[pointData.loc[domainNeighs, 'domain'] != ii, 'domain']
      counts = neighDomains.value_counts()
      frac = float(counts.max()) / (counts.sum() + len(inDomBoundary))

      if frac > threshold:
        newDom = neighDomains.value_counts().idxmax()
        pointData.loc[pointData.domain == ii, 'domain'] = newDom
        changed = True

  pointData.loc[:, 'domain'] = pd.Categorical(pointData.domain).codes # compress domain codes
  return pointData

def merge_boas_depth(pointData, threshold=0.0):
  changed = True
  while changed:
#    print "wat"
    changed = False # new round
    domains = pointData.domain.unique()

    ''' For every domain, find point of highest curvature '''
    for ii in domains:
      inDomain = np.where(pointData.domain == ii)[0]
      maxCurv = pointData.loc[inDomain, 'curv'].max()

      ''' Find all neighbouring domains that haven't already been accounted for '''
      # Find neighbouring domains
      domainNeighs = np.unique([x for y in pointData.loc[inDomain, 'neighs'].values for x in y])
      neighDomains = pointData.loc[domainNeighs, 'domain']
      neighDomains = neighDomains[neighDomains > ii].unique() # only take unvisited

      ''' Compare boundary curvatures to peak curvature to estimate basin depth (height) '''
      # Get IDs on boundary
      for jj in neighDomains:
        boundaryPoints = []
        inDomain2 = np.where(pointData.domain == jj)[0]
        for kk in inDomain2:
          for ll in pointData.loc[kk, 'neighs']:
            if ll in inDomain:
              boundaryPoints.append(kk)
              boundaryPoints.append(ll)
        boundaryPoints = np.unique(boundaryPoints)

        # Get highest boundary point (including domain neighbours), compare with peak
        boundaryMaxCurv = pointData.loc[boundaryPoints, 'curv'].max()
        diff_ = maxCurv - boundaryMaxCurv

        ''' Merge if < threshold '''
        if diff_ < threshold:
          pointData.loc[pointData.domain == jj, 'domain'] = ii
          changed = True
  pointData.loc[:, 'domain'] = pd.Categorical(pointData.domain).codes
  return pointData

def get_boundary_points(A):
    featureEdges = vtk.vtkFeatureEdges()
    featureEdges.SetInputConnection(A.mesh.GetProducerPort())
    featureEdges.BoundaryEdgesOn()
    featureEdges.FeatureEdgesOff()
    featureEdges.ManifoldEdgesOff()
    featureEdges.NonManifoldEdgesOff()
    featureEdges.Update()

    ### Get the coordinates for the points at the boundary
    toRemove = []
    edgePoints = featureEdges.GetOutput().GetPoints()
    for ii in xrange(edgePoints.GetNumberOfPoints()):
      toRemove.append(edgePoints.GetPoint(ii))

    ### Get the coordinates for all the points in the mesh
    points = []
    allPoints = A.mesh.GetPoints()
    for ii in xrange(allPoints.GetNumberOfPoints()):
      points.append(allPoints.GetPoint(ii))

    # Find the indices of the boundary points in the mesh points
    vals = [ii in toRemove for ii in points]
    indices = np.array([ii for ii, x in enumerate(vals) if x])
    return indices

# TODO: Documentation
def remove_boas_size(pointData, threshold, method = "points"):
  domainSizes = pointData.loc[:, 'domain'].value_counts()
  if method == "points":
    toRemove = domainSizes < threshold
  elif method == "relative_meristem":
    toRemove = domainSizes / domainSizes.max() < threshold
  elif method == "relative_all":
    toRemove = domainSizes / domainSizes.sum() < threshold
  removeIndices = domainSizes[toRemove].index
  pointData.loc[pointData.domain.isin(removeIndices), 'domain'] = float('NaN')
  return pointData

# TODO: Documentation
def get_nBoas(pointData):
  return pd.DataFrame(np.array(pointData['domain']))[0].value_counts().size

# TODO: Documentation
def get_boas_nPoints(pointData):
  return pd.DataFrame(np.array(pointData['domain']))[0].value_counts()

